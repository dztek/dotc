function dot {
  if [[ "${#@}" -eq "0" ]]
  then
    ls -A1 $HOME/dotc/dots
  else
    $EDITOR $HOME/dotc/dots/$@.sh
  fi
}

