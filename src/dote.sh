function dote {
  if [[ "${#@}" -eq "0" ]]
  then
    $EDITOR $HOME/dotc/main.sh 
  else
    $EDITOR $HOME/dotc/src/$@.sh
  fi
}

