export EDITOR=nvim

dotsrc=$HOME/dotc/src

source $dotsrc/dote.sh
source $dotsrc/dotr.sh
source $dotsrc/dotload.sh
source $dotsrc/dot.sh

# init the dotfiles
dotload
