# dotc
Lightweight shell-script helper and organizer

## Start
```
git clone https://bitbucket.org/dztek/dotc.git ~/dotc
```
### Add main.sh to your profile ~/.zprofile or ~/.bashrc for example
```
source ~/dotc/main.sh
```
